
#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

  cd /vagrant

  service docker start
  chmod -R 777 /var/lib/docker
  chmod 777 /var/run/docker.sock

# elasticsearch cluster up
# docker xfs persistent share to elasticsearch

  mkdir /var/lib/docker/data
  chmod -R 777 /var/lib/docker/data
  sysctl -w vm.max_map_count=262144

  docker-compose up -d

  echo "1 elastic cluster on localhost:9200"
  echo "2 kibana on localhost:5601"

# http://localhost:5601/api/status
# http://localhost:9200/*


#vpull logstash container
  docker pull docker.elastic.co/logstash/logstash:6.5.0
  docker run -d -it --rm  --net vagrant_esnet -v /vagrant/pipeline/:/usr/share/logstash/pipeline/ -p 5044:5044 --name logstash docker.elastic.co/logstash/logstash:6.5.0

echo " 3 logstash on docker logs"

#xpull filebeat container
  docker pull docker.elastic.co/beats/filebeat:6.5.0
  docker run -d -it --rm --mount type=bind,source=/vagrant/filebeat.yml,target=/usr/share/filebeat/filebeat.yml --mount type=bind,source=/var/lib/docker/containers,target=/usr/share/dockerlogs/data,readonly --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock --net vagrant_esnet --name filebeat docker.elastic.co/beats/filebeat:6.5.0

echo " 4 filebeat on docker logs"

# pull container monitoring (portainer)
  docker volume create portainer_data
  docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

  echo "5 container monitoring and orchestrating on localhost:9000"

  docker pull grafana/grafana
  docker run -d -it --rm -p 3000:3000 --mount type=bind,source=/vagrant/grafana/conf/defaults.ini,target=/usr/share/grafana/conf/defaults.ini --name=grafana -e "GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource,bessler-pictureit-panel" --net vagrant_esnet grafana/grafana

  echo "6 grafana on localhost:80 redirected admin:admin"

# start webapps
  docker pull amd64/tomcat
  docker run -d -it --mount type=bind,source=/vagrant/wars,target=/usr/local/tomcat/webapps/ --mount type=bind,source=/vagrant/tomcat/conf,target=/usr/local/tomcat/conf/ --name tomcat --rm -p 8888:8080 amd64/tomcat

  echo "7 demo app on localhost:8888/sample"

# readable all about containers
  chmod -R 777 /var/lib/docker/containers/*

  # wait for port alive

    now=$(date +"%T")
      echo -ne "Starting time : $now "
      echo waiting for port 3000 ...

  until ncat -z 127.0.0.1 3000;
  do
      now=$(date +"%T")
      echo -ne "Current time  : $now \r".

      sleep 1
  done
  echo "portcheck OK"

  #waiting for grafana up healthcheck

  until $(curl --output /dev/null --silent --head --fail http://localhost:3000/api/health); do
      printf '.'
      sleep 5
  done
  echo "healthcheck OK"
  
# elastic default search index via kibana API
  curl --verbose -POST http://localhost:5601/api/saved_objects/index-pattern -H "kbn-version: 6.5.0" -H 'Content-Type: application/json;charset=UTF-8' -H 'User-Agent: Mozilla/5.0' -H 'Accept: Application/json, text/plain, /' -H 'DNT:1' -d '{"attributes":{"title":"*","timeFieldName":"@timestamp","notExpandable":true}}'

# parsing log with elastic_ingest API
  curl -H 'Content-Type: application/json' -XPUT 'http://localhost:9200/_ingest/pipeline/pipeline-1' -d@ingest_pipeline/pipeline-1.json
  curl -H 'Content-Type: application/json' -XPUT 'http://localhost:9200/_ingest/pipeline/pipeline-11' -d@ingest_pipeline/pipeline-11.json

# set dashboard and datasource via grafana API
  curl -X "POST" "http://localhost:3000/api/datasources" -H "Content-Type: application/json" --user admin:admin --data-binary @datasource.json
# add dashboard
  curl -i -u admin:admin -H "Content-Type: application/json" -X POST http://localhost:3000/api/dashboards/db -d@dashboard_up.json
# export dashboard
  curl -i -u admin:admin -H "Content-Type: application/json" -X POST http://localhost:3000/api/dashboards/import -d@dashboard.json
# set home dashboard
  curl -i -u admin:admin -H "Content-Type: application/json" -XPUT localhost:3000/api/org/preferences -d@dashboard_home.json
# exporting dashbord use chrome developer export curl option..
