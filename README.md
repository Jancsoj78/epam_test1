# DevOps Art

[![N|Solid](https://gitlab.com/Jancsoj78/epam_test1/raw/d8d9d673e2831b9e70e3ea1b8da4c9ed32a86735/dev-art-infra.PNG)](https://gitlab.com/Jancsoj78/epam_test1)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

infra as a code project.

  -Prereq:
- Virtualbox
- Vagrant
- Powershell (>2)
- 2 core 8G with CPU virtualization enable.
-
### Installation
Install the dependencies and devDependencies and start the server.

```sh
$ vagrant destroy
$ vagrant up
```
### Don't use latest tag on Production
- After restart virtualbox containers up again
```
